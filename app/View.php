<?php

echo <<< _END
<h1>Podaj skladniki na podstawie których wyszukam przepisy.</h1>
<p>Dostępne składniki: papryka, cukinia, kurczak, przyprawa do kurczaka, pieprz, sól, olej, kiełbasa, papryka słodka, ziemniaki, marchew, ryba dorsz, koncentrat, cebula </p>
      <form action="">
         Skladnik 1 : <input type="text"  id="a">  <br>
         <s>Skladnik 2 : </s> <input type="text"  id="b">  <br>
         <s>Skladnik 3</s> : <input type="text"  id="c">  <br>
         <s>Skladnik 4</s> : <input type="text"  id="d">  <br>
         <s>Skladnik 5</s> : <input type="text"  id="e">  <br>
         <button id="wyslij" type="button">Zatwierdz wybór (wyświetla bez przeładowania strony)</button>
      </form>

      <script>
         $(document).ready(function ()
         {
            $("#wyslij").click(function ()
            {
               var a = $('#a').val();
               var b = $('#b').val();
               var c = $('#c').val();
               var d = $('#d').val();
               var e = $('#e').val();
               $.ajax(
                       {
                          type: "post",
                          url: "app/Model.php",
                          data: {key1: a, key2: b, key3: c, key4: d, key5: e},
                          //dataType:"json",
                          success: function (data, status)
                          {
                             $('#info').html(data);
                             //alert('Status przeładowania : '+status);
                          },
                          error: function (err) {
                             alert("Wystąpił błąd");
                             console.log(err);
                          }
                       });
            });
         });
      </script>

      <h1>Wczytywanie przepisów do znacznika DIV</h1>
      <div id="info">Po wybraniu produktów w tym miejscu wyświetlą się wybrane dla Ciebie przepisy.</div>
      <br>

_END;
