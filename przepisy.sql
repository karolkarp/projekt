-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 05 Gru 2016, 13:11
-- Wersja serwera: 10.1.16-MariaDB
-- Wersja PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projekt`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy`
--

CREATE TABLE `przepisy` (
  `id` int(11) NOT NULL,
  `nazwa` tinytext COLLATE utf8_polish_ci NOT NULL,
  `skladniki` text COLLATE utf8_polish_ci NOT NULL,
  `ilosc` tinyint(4) NOT NULL,
  `przepis` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `przepisy`
--

INSERT INTO `przepisy` (`id`, `nazwa`, `skladniki`, `ilosc`, `przepis`) VALUES
(1, 'Przepis na leczo.', 'papryka, cukinia, pieprz, sól, olej, kiełbasa, cebula, papryka słodka, ziemniaki, ', 10, 'obierz dodaj, pokrój dodaj, wymieszaj, gotuj 45 min, '),
(2, 'Przepis na rybę po grecku.', 'marchew, ryba dorsz, koncentrat, cebula', 4, 'pokrój dodaj, wymieszaj, gotuj 50 min, '),
(3, 'Ziemniaki pieczone', 'ziemniaki', 1, 'obrać, upiec'),
(4, 'Pieczony Kurczak', 'kurczak, przyprawa do kurczaka', 2, 'przyprawić, upiec');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `przepisy`
--
ALTER TABLE `przepisy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
