<?php
//namespace Conn;
class ConnDB
{
   public function connect()
   {
      global $fromBase;
      global $pdo;
      try
      {
         $pdo = new PDO('mysql:host=localhost;dbname=projekt;charset=utf8', 'root', '');
         echo 'Connected to DataBase succesfully!'."<br>";
         $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

         $sql = 'SELECT id, nazwa, skladniki, ilosc, przepis FROM przepisy';
         $fromBase = $pdo->query($sql);
         //var_dump($fromBase);
      }
      catch(PDOException $e)
      {
         echo 'Error in connection with DataBase: ' . $e->getMessage;
      }  
   }
}
      